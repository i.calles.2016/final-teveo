from django.db import models


# Create your models here.
class Cam_L1(models.Model):
    cam_id = models.CharField(max_length=10, primary_key=True)
    source = models.URLField()
    place = models.CharField(max_length=100)
    coordenates = models.CharField(max_length=50)

    def full_cam_id(self):
        return f'LIS1-{self.cam_id}'

class Cam_L2(models.Model):
    cam_id = models.CharField(max_length=10, primary_key=True)
    source = models.URLField()
    place = models.CharField(max_length=100)
    coordenates = models.CharField(max_length=50)

    def full_cam_id(self):
        return f'LIS2-{self.cam_id}'

class Comment_L1(models.Model):
    cam = models.ForeignKey(Cam_L1, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    img = models.ImageField()

    class Meta:
        ordering = ['-timestamp']


class Comment_L2(models.Model):
    cam = models.ForeignKey(Cam_L2, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    img = models.ImageField(upload_to='images/', null=True, blank=True)

    class Meta:
        ordering = ['-timestamp']


class Counter(models.Model):
    comments_value = models.IntegerField(default=0)
    cameras_value = models.IntegerField(default=0)