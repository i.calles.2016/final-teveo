from django.test import TestCase
from django.urls import reverse
from .models import Cam_L1, Cam_L2, Comment_L1, Comment_L2

class CameraViewsTests(TestCase):
    def setUp(self):
        # Setup objects for testing
        self.cam_l1 = Cam_L1.objects.create(cam_id='test_cam_l1', source='http://example.com', place='Test Place 1',
                                            coordenates='1.234,5.678')
        self.cam_l2 = Cam_L2.objects.create(cam_id='test_cam_l2', source='http://example.com', place='Test Place 2',
                                            coordenates='3.456,7.890')
        self.comment_l1 = Comment_L1.objects.create(cam=self.cam_l1, text='Test Comment 1', img='test_image1.jpg')
        self.comment_l2 = Comment_L2.objects.create(cam=self.cam_l2, text='Test Comment 2', img='test_image2.jpg')

    def test_index_page(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'app/principal.html')

    def test_help_page(self):
        url = reverse('ayuda')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'app/ayuda.html')

    def test_cameras_page(self):
        url = reverse('camaras')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'app/camaras.html')

    def test_configuration_page(self):
        url = reverse('conf')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'app/conf.html')

    def test_camera_details_page(self):
        # Assuming you have a camera with ID 'test_camera_id'
        camera_id = self.cam_l1.cam_id
        url = reverse('camara_id', kwargs={'id': camera_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # Adjust the template name as per your implementation
        self.assertTemplateUsed(response, 'app/camara.html')

    def test_camara_json_view(self):
        # Test JSON response for Cam_L1
        url = reverse('camara_json') + '?id=test_cam_l1'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.json()['cam_id'], 'test_cam_l1')

        # Test JSON response for Cam_L2
        url = reverse('camara_json') + '?id=test_cam_l2'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.json()['cam_id'], 'test_cam_l2')

        # Test JSON response for non-existent camera
        url = reverse('camara_json') + '?id=non_existent'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(response.json()['error'], 'Cámara no encontrada')

    def test_comentario_view(self):
        url = reverse('comentario')
        response = self.client.get(url, {'id': self.cam_l1.cam_id})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'app/comentario.html')

        # Test POST request to the comentario view
        data = {'id': self.cam_l1.cam_id, 'texto': self.comment_l1.text}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


