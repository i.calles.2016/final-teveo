from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import urllib.request, time, os, uuid, random
from django.conf import settings
import xml.etree.ElementTree as ET
from PIL import Image
from io import BytesIO
from urllib.request import urlopen
from django.http import JsonResponse
from .forms import ConfigForm
from .models import Cam_L1, Cam_L2, Comment_L1, Comment_L2, Counter
from django.db import IntegrityError


# Create your views here.
def index(request):
    # Obtener todos los comentarios de ambos modelos
    comments_l1 = Comment_L1.objects.all()
    comments_l2 = Comment_L2.objects.all()

    # Unir y ordenar los comentarios por fecha de más reciente a más antiguo
    all_comments = sorted(
        list(comments_l1) + list(comments_l2),
        key=lambda comment: comment.timestamp,
        reverse=True
    )
    # Configurar paginación, por ejemplo, 2 comentarios por página
    paginator = Paginator(all_comments, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    comments_value, cameras_value = footer()
    return render(request, 'app/principal.html',
                  {'page_obj': page_obj, 'comments_counter': comments_value, 'cameras_counter': cameras_value})


def camaras(request):
    if request.method == 'POST':
        if 'download_listado1' in request.POST:
            download_data_from_listado1()
        elif 'download_listado2' in request.POST:
            download_data_from_listado2()
        return redirect('camaras')

    random_camera, cameras = camaras_page()
    comments_value, cameras_value = footer()

    paginator = Paginator(cameras, 4)
    page = request.GET.get('page', 1)

    try:
        cameras = paginator.page(page)
    except PageNotAnInteger:
        cameras = paginator.page(1)
    except EmptyPage:
        cameras = paginator.page(paginator.num_pages)

    return render(request, 'app/camaras.html', {
        'cameras': cameras,
        'random_camera': random_camera,
        'comments_counter': comments_value,
        'cameras_counter': cameras_value
    })


def generate_session_id(request):
    if not request.session.get('session_id'):
        request.session['session_id'] = str(uuid.uuid4())


def authorize(request):
    session_id = request.GET.get('id')
    if session_id:
        # Copiar la configuración de la sesión identificada por session_id a la sesión actual
        # Aquí se pueden copiar variables de configuración de la sesión original
        original_session = request.session
        for key, value in original_session.items():
            request.session[key] = value

    return redirect('index')


def conf(request):
    generate_session_id(request)
    session_id = request.session['session_id']
    comments_value, cameras_value = footer()

    if request.method == 'POST':
        form = ConfigForm(request.POST)
        if form.is_valid():
            request.session['commentor_name'] = form.cleaned_data['commentor_name']
            request.session['font_size'] = form.cleaned_data['font_size']
            request.session['font_type'] = form.cleaned_data['font_type']
            return redirect('conf')
    else:
        initial_data = {
            'commentor_name': request.session.get('commentor_name', ''),
            'font_size': request.session.get('font_size', 'medium'),
            'font_type': request.session.get('font_type', 'sans-serif')
        }
        form = ConfigForm(initial=initial_data)
        return render(request, 'app/conf.html',
                      {'session_id': session_id, 'form': form, 'comments_counter': comments_value,
                       'cameras_counter': cameras_value})


def ayuda(request):
    comments_value, cameras_value = footer()
    return render(request, 'app/ayuda.html', {'comments_counter': comments_value, 'cameras_counter': cameras_value})


def camara_id(request, id):
    comments_value, cameras_value = footer()
    try:
        camera = Cam_L1.objects.get(cam_id=id)
        if camera:
            comments = Comment_L1.objects.filter(cam=camera).order_by('-timestamp')
            return render(request, 'app/camara.html',
                          {'camera': camera, 'comments': comments, 'comments_counter': comments_value,
                           'cameras_counter': cameras_value})
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
            if camera:
                comments = Comment_L2.objects.filter(cam=camera).order_by('-timestamp')
                return render(request, 'app/camara.html',
                              {'camera': camera, 'comments': comments, 'comments_counter': comments_value,
                               'cameras_counter': cameras_value})
        except Cam_L2.DoesNotExist:
            return render(request, 'app/error.html')


def camara_json(request):
    id = request.GET.get('id') or request.POST.get('id')
    print(id)
    try:
        camera = Cam_L1.objects.get(cam_id=id)
        if camera:
            comments_count = Comment_L1.objects.filter(cam=camera).count()
            data = {
                'cam_id': camera.cam_id,
                'source': camera.source,
                'place': camera.place,
                'coordenates': camera.coordenates,
                'comments_count': comments_count
            }
            return JsonResponse(data)
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
            if camera:
                comments_count = Comment_L2.objects.filter(cam=camera).count()
                data = {
                    'cam_id': camera.cam_id,
                    'source': camera.source,
                    'place': camera.place,
                    'coordenates': camera.coordenates,
                    'comments_count': comments_count
                }
                return JsonResponse(data)
        except Cam_L2.DoesNotExist:
            return JsonResponse({'error': 'Cámara no encontrada'}, status=404)


def comentario(request):
    comments_value, cameras_value = footer()
    id = request.GET.get('id') or request.POST.get('id')

    try:
        camera = Cam_L1.objects.get(cam_id=id)
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
        except Cam_L2.DoesNotExist:
            return render(request, 'app/error.html')

    if request.method == 'POST':
        texto = request.POST.get('texto')
        commentor_name = request.session.get('commentor_name', 'Anónimo')

        try:
            camera = Cam_L1.objects.get(cam_id=id)
            if camera and texto:
                img_name = f"comment_{id}_{int(time.time())}.jpg"
                img_path = save_image_from_url(camera.source, img_name)

                if img_path is None:
                    img_path = 'comments/default.jpg'

                comment = Comment_L1(cam=camera, text=f'{commentor_name}: {texto}', img=img_path)
                comment.save()

                counter, created = Counter.objects.get_or_create(id=1)
                counter.comments_value += 1
                counter.save()

                return redirect('camara_id', id=camera.cam_id)
        except Cam_L1.DoesNotExist:
            try:
                camera = Cam_L2.objects.get(cam_id=id)
                if camera and texto:
                    # Descargar la imagen y guardarla en la carpeta media
                    img_name = f"comment_{id}_{int(time.time())}.jpg"
                    img_path = save_image_from_url(camera.source, img_name)

                    if img_path is None:
                        # Si hay un error en la descarga, usa una imagen predeterminada
                        img_path = 'comments/default.jpg'

                    comment = Comment_L2(cam=camera, text=f'{commentor_name}: {texto}', img=img_path)
                    comment.save()

                    # Actualizar el contador de comentarios
                    counter, created = Counter.objects.get_or_create(id=1)
                    counter.comments_value += 1
                    counter.save()

                    return redirect('camara_id', id=camera.cam_id)
            except Cam_L2.DoesNotExist:
                pass
                return render(request, 'app/error.html')

    return render(request, 'app/comentario.html',
                  {'camera': camera, 'comments_counter': comments_value, 'cameras_counter': cameras_value})


def camara_dyn(request, id):
    comments_value, cameras_value = footer()
    try:
        camera = Cam_L1.objects.get(cam_id=id)
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
        except Cam_L2.DoesNotExist:
            return render(request, 'app/error.html')

    return render(request, 'app/camara-dyn.html', {
        'camera': camera,
        'comments_counter': comments_value,
        'cameras_counter': cameras_value
    })


def camara_image(request, id):
    try:
        camera = Cam_L1.objects.get(cam_id=id)
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
        except Cam_L2.DoesNotExist:
            return render(request, 'app/error.html')

    return render(request, 'app/partials/camara_image.html', {'camera': camera})


def camara_comments(request, id):
    try:
        camera = Cam_L1.objects.get(cam_id=id)
        comments = Comment_L1.objects.filter(cam=camera).order_by('-timestamp')
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
            comments = Comment_L2.objects.filter(cam=camera).order_by('-timestamp')
        except Cam_L2.DoesNotExist:
            return render(request, 'error.html')

    return render(request, 'app/partials/camara_comments.html', {'comments': comments})


def comment_form(request, id):
    try:
        camera = Cam_L1.objects.get(cam_id=id)
    except Cam_L1.DoesNotExist:
        try:
            camera = Cam_L2.objects.get(cam_id=id)
        except Cam_L2.DoesNotExist:
            return render(request, 'error.html')

    return render(request, 'app/partials/comment_form.html', {'camera': camera})


def download_data_from_listado1():
    url = "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml"
    with urllib.request.urlopen(url) as response:
        xml_content = response.read()

    root = ET.fromstring(xml_content)
    for camara_elem in root.findall('camara'):
        cam_id = camara_elem.find('id').text
        source = camara_elem.find('src').text
        place = camara_elem.find('lugar').text
        coordenates = camara_elem.find('coordenadas').text

        if not Cam_L1.objects.filter(cam_id=cam_id).exists():
            try:
                Cam_L1.objects.create(
                    cam_id=cam_id,
                    source=source,
                    place=place,
                    coordenates=coordenates
                )
            except IntegrityError:
                print(f"Error: La cámara con ID {cam_id} ya existe en la base de datos.")


def download_data_from_listado2():
    url = "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml"
    with urllib.request.urlopen(url) as response:
        xml_content = response.read()

    root = ET.fromstring(xml_content)
    for camara_elem in root.findall('cam'):
        cam_id = camara_elem.get('id')
        source = camara_elem.find('url').text
        place = camara_elem.find('info').text
        latitude = camara_elem.find('place/latitude').text
        longitude = camara_elem.find('place/longitude').text
        coordenates = f"{latitude},{longitude}"

        if not Cam_L2.objects.filter(cam_id=cam_id).exists():
            try:
                Cam_L2.objects.create(
                    cam_id=cam_id,
                    source=source,
                    place=place,
                    coordenates=coordenates
                )
            except IntegrityError:
                print(f"Error: La cámara con ID {cam_id} ya existe en la base de datos.")


def camaras_page():
    cameras_l1 = Cam_L1.objects.all()
    cameras_l2 = Cam_L2.objects.all()

    all_cameras = []
    for camera in cameras_l1:
        comments_count = Comment_L1.objects.filter(cam=camera).count()
        all_cameras.append({
            'camera': camera,
            'comments_count': comments_count
        })

    for camera in cameras_l2:
        comments_count = Comment_L2.objects.filter(cam=camera).count()
        all_cameras.append({
            'camera': camera,
            'comments_count': comments_count
        })

    all_cameras.sort(key=lambda x: x['comments_count'], reverse=True)
    random_camera = random.choice(all_cameras) if all_cameras else None

    return random_camera, all_cameras


def footer():
    comments_value = increment_comment_value()
    cameras_value = cameras_count()

    return comments_value, cameras_value


def increment_comment_value():
    if not Counter.objects.exists():
        Counter.objects.create(comments_value=0)

    # Contar el número de comentarios en el modelo Cam_L1
    comments_l1_count = Comment_L1.objects.count()
    # Contar el número de comentarios en el modelo Cam_L2
    comments_l2_count = Comment_L2.objects.count()
    # Sumar los resultados
    total_comments = comments_l1_count + comments_l2_count
    # Actualizar el valor en la clase Counter
    counter = Counter.objects.get(id=1)
    counter.comments_value = total_comments
    counter.save()

    return counter.comments_value


def cameras_count():
    if not Counter.objects.exists():
        Counter.objects.create(cameras_value=0)
    # Contar el número de cámaras en el modelo Cam_L1
    cam_l1_count = Cam_L1.objects.count()
    # Contar el número de cámaras en el modelo Cam_L2
    cam_l2_count = Cam_L2.objects.count()
    # Sumar los resultados
    total_cameras = cam_l1_count + cam_l2_count
    # Actualizar el valor en la clase Counter
    counter = Counter.objects.get(id=1)
    counter.cameras_value = total_cameras
    counter.save()

    return counter.cameras_value


def save_image_from_url(url, img_name):
    try:
        response = urlopen(url)
        if response.status == 200:
            img_data = response.read()

            # Verificar si es una imagen válida
            try:
                img = Image.open(BytesIO(img_data))
                img.verify()  # Verifica que es una imagen válida

                # Guardar la imagen
                img = Image.open(BytesIO(img_data))
                img_path = os.path.join(settings.MEDIA_ROOT, 'comments', img_name)
                img.save(img_path)
                return f'comments/{img_name}'
            except (IOError, SyntaxError) as e:
                print(f"Error verifying image: {e}")
                return None
    except Exception as e:
        print(f"Error downloading image: {e}")
        return None
    return None
