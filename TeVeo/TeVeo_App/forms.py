from django import forms

class ConfigForm(forms.Form):
    commentor_name = forms.CharField(label='Nombre del comentador', max_length=100)
    font_size = forms.ChoiceField(
        label='Tamaño de la letra',
        choices=[
            ('small', 'Pequeña'),
            ('medium', 'Mediana'),
            ('large', 'Grande')
        ]
    )
    font_type = forms.ChoiceField(
        label='Tipo de letra',
        choices=[
            ('sans-serif', 'Sans-serif'),
            ('serif', 'Serif'),
            ('monospace', 'Monoespaciada')
        ]
    )
