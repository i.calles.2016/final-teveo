"""items URL Configuration
"""

from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('Camaras', views.camaras, name='camaras'),
    path('Configuracion', views.conf, name='conf'),
    path('Ayuda', views.ayuda, name='ayuda'),
    path('Camara/<str:id>', views.camara_id, name='camara_id'),
    path('Camara/<str:id>/dyn', views.camara_dyn, name="camara_dyn"),
    path('Comentario', views.comentario, name='comentario'),
    path('authorize/', views.authorize, name='authorize'),
    path('json', views.camara_json, name='camara_json'),
    path('Camara/<str:id>/image', views.camara_image, name='camara_image'),
    path('Camara/<str:id>/comments', views.camara_comments, name='camara_comments'),
    path('Camara/<str:id>/comment_form', views.comment_form, name='comment_form'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)