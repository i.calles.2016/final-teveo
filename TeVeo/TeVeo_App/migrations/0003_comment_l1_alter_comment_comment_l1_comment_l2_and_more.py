# Generated by Django 5.0.3 on 2024-05-14 10:08

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("TeVeo_App", "0002_cam_l2_counter_comment"),
    ]

    operations = [
        migrations.CreateModel(
            name="Comment_L1",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("timestamp", models.DateTimeField(auto_now_add=True)),
                ("text", models.TextField()),
                (
                    "cam_L1",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="TeVeo_App.cam_l1",
                    ),
                ),
            ],
            options={
                "ordering": ["-timestamp"],
            },
        ),
        migrations.AlterField(
            model_name="comment",
            name="comment_L1",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="TeVeo_App.comment_l1"
            ),
        ),
        migrations.CreateModel(
            name="Comment_L2",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("timestamp", models.DateTimeField(auto_now_add=True)),
                ("text", models.TextField()),
                (
                    "cam_L2",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="TeVeo_App.cam_l2",
                    ),
                ),
            ],
            options={
                "ordering": ["-timestamp"],
            },
        ),
        migrations.AlterField(
            model_name="comment",
            name="comment_L2",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="TeVeo_App.comment_l2"
            ),
        ),
    ]
