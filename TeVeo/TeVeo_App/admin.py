from django.contrib import admin
from .models import Cam_L1, Cam_L2, Comment_L1, Comment_L2, Counter
admin.site.register(Cam_L1)
admin.site.register(Cam_L2)
admin.site.register(Comment_L1)
admin.site.register(Comment_L2)
admin.site.register(Counter)
# Register your models here.
