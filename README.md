# Final-TeVeO

# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Irene Calles Sánchez
* Titulación: Ingeniería en Sistemas de Telecomunicación
* Cuenta en laboratorios: icalles
* Cuenta URJC: i.calles.2016@alumnos.urjc.es
* Video básico (url): https://youtu.be/0okw0npRGv0
* Video parte opcional (url): https://youtu.be/IE0E6yS_bfE
* Despliegue (url): http://icalles.pythonanywhere.com/
* Contraseñas: TeVeoFinal
* Cuenta Admin Site: icalles/TeVeoFinal

## Resumen parte obligatoria
* Al acceder a la página web, en la página principal se muestran los comentarios introducidos por los comentadores, organizados de más nuevos a más viejos. Para cada comentario se mostrará la información de esa cámara.
* Al acceder la página de cámaras se muestra un listado de las dos fuentes de datos disponibles y un listado de las cámaras ordenadas por número de comentarios (de más a menos).
* En la página de cada cámara aparece un enlace a la página dinámica, un enlace a la página json, información de la cámara (nombre y localización), imagen de la cámara actual, un listado de todos los comentarios puestos para esa cámara y un enlace a la página que permite poner comentarios.
* En la página dinámica aparece el mismo contenido que en la página de la cámara. Cada 30 segundos se actualiza la imagen de la cámara.
* En la página json se muestra la información de la cámara y el numero de comentarios en formato JSON.
* En la página de configuración aparecen un formulario para elegir el nombre del comentador, un formulario para elegir el tipo y el tamaño de la letra y un enlace de autenticación.
* En la página de ayuda encontramos una breve explicación sobre el funcionamiento de la web.
* En la página de acceso al Admin Site aparece un formulario para iniciar sesión, introducimos el nombre de usuario y la contraseña y nos redirige a la página de administración de Django.
* En todas las páginas se encuentra un menú  de navegación y un pie de página, el cual indica la cantidad todal de camaras y comentarios que hay.

## Lista partes opcionales

* Inclusión de un favicon del sitio.
* Paginación. En la página principal se puede observar como están paginados los comentarios Por otro lado, en la página de cámaras el listado de cámaras tambien aparece paginado.
